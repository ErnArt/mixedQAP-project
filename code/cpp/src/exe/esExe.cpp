#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <random>

#include <solution.h>
#include <MixedQAPEval.h>
#include <randomPermutation.h>
#include <ES.h>
#include <FullNeighborhoodMixedQAPEval.h>
#include <uniformContinue.h>

using namespace std;

/**
 *  Main function
 */
int main(int argc, char **argv) {
	char * fileName = argv[1];
	unsigned int seed = atoi(argv[2]);
	unsigned int duration = atoi(argv[3]);

	auto rng = std::default_random_engine {};
	rng.seed(seed);

	MixedQAPEval eval(fileName);
	RandomPermutation init(rng, eval.n);
	UniformContinue uniform(eval.n);
	ES search(eval, eval.n, rng);

	std::cout.precision(15);

	Solution solution(eval.n);
	init(solution);
	uniform(solution);
	eval(solution);
	search.timeLimit(time(NULL) + duration);
	search(solution);
	cout << solution << endl;
	return 1;
}
