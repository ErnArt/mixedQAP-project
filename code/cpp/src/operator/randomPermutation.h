#ifndef _randomPermutation_h
#define _randomPermutation_h

#include "operator.h"
#include "solution.h"
#include <random>
#include <algorithm>

class RandomPermutation : public Operator {
	public:
		RandomPermutation(std::default_random_engine & _rng, unsigned int _n) : rng(_rng), n(_n) {}

		void operator()(Solution & solution) {
			

			std::uniform_int_distribution<int> uniDist(0, solution.sigma.size()-1);
			int i = uniDist(rng);
			int j = uniDist(rng);

			while (j == i)
				j = uniDist(rng);

			std::swap(solution.sigma[i], solution.sigma[j]);
		}
	
	protected:
		unsigned int n;

		std::default_random_engine & rng;
};

#endif