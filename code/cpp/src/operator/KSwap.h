#ifndef _KSwap_h
#define _KSwap_h

#include "operator.h"
#include "solution.h"
#include <random>
#include <algorithm>
#include <randomPermutation.h>

class KSwap : public Operator {
	public:
		KSwap(std::default_random_engine & _rng, unsigned int _n) : rng(_rng), n(_n) { }

		void operator()(Solution & solution) {
			std::uniform_int_distribution<int> uniDist(1, (n-1)/2);
			int kmax = uniDist(rng);

			std::uniform_int_distribution<int> secondUniDist(0, kmax-1);
			int kmin = secondUniDist(rng);
			RandomPermutation init(rng, solution.sigma.size());

			for (int i=kmin; i<kmax; i++)
				init(solution);
		}
	
	protected:
		unsigned int n;

		std::default_random_engine & rng;
};

#endif