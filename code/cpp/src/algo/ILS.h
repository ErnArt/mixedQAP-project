#include "search.h"
#include "MixedQAPEval.h"
#include <randomPermutation.h>
#include "NeighborhoodMixedQAPEval.h"
#include "KSwap.h"
#include <vector>
#include <iostream>
#include <random>

class ILS : public Search {
	public:
		ILS(MixedQAPEval & _eval, NeighborhoodMixedQAPEval & _neighborhoodMixedQAPEval, unsigned _n, std::default_random_engine & _rng): eval(_eval), neighborhoodMixedQAPEval(_neighborhoodMixedQAPEval), n(_n), rng(_rng) {
			delta.resize(n);

			for (unsigned int i=0; i<n; i++) {
				delta[i].resize(n);
			}
		}

		virtual void operator() (Solution & _solution) {
			KSwap kswap(rng, _solution.sigma.size());

			std::pair<unsigned int, unsigned int> move;
			int cpt=0;

			neighborhoodMixedQAPEval.init(_solution, delta);

			while(time(NULL) < timeLimit_) {
				cpt++;

				kswap(_solution);

				unsigned tmp = _solution.sigma[move.first];
				_solution.sigma[move.first] = _solution.sigma[move.second];
				_solution.sigma[move.second] = tmp;
				_solution.fitness += delta[move.first][move.second];
				//neighborhoodMixedQAPEval.update(_solution, move, delta);
				eval(_solution);
				
				selectBestNeighbor(move);
			}

			std::cout<<"Count = "<<cpt<<std::endl;
		}

	protected:
		virtual void selectBestNeighbor(std::pair<unsigned int, unsigned int> & best) {
			unsigned int i, j;

			best.first = 1;
			best.second = 0;
			double bestDelta = delta[best.first][best.second];

			for (i=1; i<n; i++) {
				for (j=0; j<i; j++) {
					if (delta[i][j] < bestDelta) {
						best.first = i;
						best.second = j;
						bestDelta = delta[i][j];	
					}
				}
			}
		}

		MixedQAPEval & eval;
		NeighborhoodMixedQAPEval & neighborhoodMixedQAPEval;
		unsigned n;
		std::vector<std::vector<double>> delta;
		std::default_random_engine & rng;
};