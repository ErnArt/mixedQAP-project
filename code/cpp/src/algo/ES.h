#include "search.h"
#include "MixedQAPEval.h"
#include "NeighborhoodMixedQAPEval.h"
#include <vector>
#include <iostream>
#include <cmath>
#include <random>

class ES : public Search {
	public:
		ES(MixedQAPEval & _eval, unsigned _n, std::default_random_engine & _rng): eval(_eval), n(_n), rng(_rng) { }

		virtual void operator() (Solution & _solution) {
			std::pair<unsigned int, unsigned int> move;
			int cpt=0;
			double sigma=0.5;
			double gamma=2;
			// First step of repair.

			int turnWhereLower = 0;
			std::uniform_real_distribution<double> uniDist(-1.0, 1.0);

			double previousFitness = 0;

			while(turnWhereLower < 200) {
				cpt++;

				// Cloning the current solution.
				Solution copyOfSolution(_solution);
				std::vector<double> variationArray;
				for (int i=0; i<_solution.x.size(); i++) {
					variationArray.push_back(uniDist(rng));
					copyOfSolution.x.at(i) = _solution.x.at(i) + sigma * variationArray.at(i);
				}

				// First step of repair.
				for (auto & element : copyOfSolution.x)
					if (element < 0)
						element *= -1;

				// We calculate the sum to do the two distinct variation.
				double sumOfSolution = 0;
				for(auto & element : copyOfSolution.x)
					sumOfSolution += element;
					
				if (sumOfSolution == 0) 
					for(auto & element : copyOfSolution.x)
						element = 1/copyOfSolution.x.size();

				else if (sumOfSolution != 1)
					for (auto & element : copyOfSolution.x)
						element = element / sumOfSolution;

				copyOfSolution.modifiedX = true;
				eval(_solution);
				eval(copyOfSolution);

				// If the solution is better we change it and adapt our sigma, else we change in a more signficant way our sigma.
				if (copyOfSolution.fitness < _solution.fitness) {
					_solution = copyOfSolution;
					sigma *= gamma;
				}
				else
					sigma *= pow(gamma, -1.0/4.0);

				// If we have one case where the variation is not lower than 10^-6 we reinitialized the counter.
				if (std::abs(_solution.fitness - previousFitness) >= pow(10, -6))
					turnWhereLower = 0;
				else
					turnWhereLower++;

				// At the end of the loop we update our previousFitness
				previousFitness = _solution.fitness;
			}

			std::cout<<"Count = "<<cpt<<std::endl;
		}

	protected:
		MixedQAPEval & eval;
		unsigned n;
		std::default_random_engine & rng;
};