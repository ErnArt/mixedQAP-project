#ifndef _search_h
#define _search_h

#include <solution.h>


/******************************************************
 * Abstract class which perform a search
 */
class Search {
public:
	// constructor
	Search()  { }

	// vrtual method to perform the search
	virtual void operator()(Solution & _solution) = 0 ;

	virtual void timeLimit(time_t limit) {
		timeLimit_ = limit;
	}

protected:
	// the stopping criterium
	time_t timeLimit_;

	// JAVA: long startTime = System.currentTimeMillis();
};


#endif
