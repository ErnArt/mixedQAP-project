#ifndef _RandomSearch_h
#define _RandomSearch_h

#include <random>

#include <iostream>
#include <solution.h>
#include <MixedQAPEval.h>
#include <search.h>
#include <randomPermutation.h>
#include <uniformContinue.h>

/******************************************************
 * Random Search
 * 
 *
 */
class RandomSearch : public Search {
public:
	/** constructor
	 *
	 * @param _eval evaluation function
	 * @param 
	 */
	RandomSearch(std::default_random_engine & _rng, MixedQAPEval & _eval) : rng(_rng), eval(_eval) { 
	}

	// the search
	virtual void operator()(Solution & _solution) {
		RandomPermutation random(rng, _solution.sigma.size());
		UniformContinue uniform(_solution.x.size());
		int cpt=0;

		Solution s(_solution.sigma.size());
		uniform(s);

		while(time(NULL) < timeLimit_) {
			cpt++;
			random(s);

			eval(s);

			if (s.fitness < _solution.fitness)
				_solution = s;
		}

		std::cout<<cpt<<std::endl;
	}

protected:
	// pseudo-random generator 
	std::default_random_engine & rng;

	// evaluation function
	MixedQAPEval & eval;   

};


#endif
