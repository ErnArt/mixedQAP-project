import csv
import numpy as np
import pandas as pd
from pprint import pprint

# Function to create a list of all instances and give them all their count and fitness values.
def creationOfLists(fileName):
	# Creation of a list of all distincts instances removing all duplicates.
	file = pd.read_csv(fileName)
	listInstances = list(dict.fromkeys(file["instance"].values))
	instancesValues = {}

	# Adding an array in all of the distinct entries.
	for instance in listInstances:
		instancesValues[instance] = []

	# Appending all small dictionnaries of count and fitness into the previous array, sorted by instances.
	for element in file.values:
		instancesValues[element[0]].append({"count": element[2], "fitness": element[3]})

	return instancesValues

# Function to process and add all count and fitness averages for each instances with the type of algorithm used.
def averageByInstances(averageArrayInstances, instancesValues, algoType):
	# We create an empty array if there is not already one for making this function usable with multiple algorithm 
	# types and keeping all values neatly organized.
	for instance in instancesValues:
		countAverage, fitnessAverage = averageForArray(np.array(instancesValues[instance]))
		if (not instance in averageArrayInstances.keys()):
			averageArrayInstances[instance] = []
		averageArrayInstances[instance].append({"algoType":algoType, "countAverage":countAverage, "fitnessAverage": fitnessAverage})
		
	return averageArrayInstances

# Function to process a single array and return the average for count and fitness. Used per instances.
def averageForArray(instancesArray):
	countSum = 0
	fitnessSum = 0
	for element in instancesArray:
		countSum += element["count"]
		fitnessSum += element["fitness"]
	return np.round(countSum/len(instancesArray), 2), np.round(fitnessSum/len(instancesArray), 2)

# Function to process the whole array and return the average for count and fitness for all instances.
def averageForAlgo(averageArrayInstances):
	countSum = {}
	fitnessSum = {}
	for instance in averageArrayInstances:
		for element in averageArrayInstances[instance]:
			if (not element["algoType"] in fitnessSum.keys()):
				countSum[element["algoType"]] = 0
			if (not element["algoType"] in fitnessSum.keys()):
				fitnessSum[element["algoType"]] = 0
			countSum[element["algoType"]] += element["countAverage"]
			fitnessSum[element["algoType"]] += element["fitnessAverage"]

	for element in countSum:
		countSum[element] = np.round(countSum[element] / len(averageArrayInstances), 2)
	for element in fitnessSum:
		fitnessSum[element] = np.round(fitnessSum[element] / len(averageArrayInstances), 2)

	return [countSum, fitnessSum]

def main():
	averageArrayInstances = {}
	averageByInstances(averageArrayInstances, creationOfLists("resultRandomSearch.csv"), "RandomSearch")
	averageByInstances(averageArrayInstances, creationOfLists("resultHillClimber.csv"), "HillClimber")
	averageByInstances(averageArrayInstances, creationOfLists("resultILS.csv"), "ILS")
	averageByInstances(averageArrayInstances, creationOfLists("resultES.csv"), "ES")

	# Printing for each files all their average count and fitness per algorithm.
	pprint(averageArrayInstances)

	# Printing the overall average of each algorithm, sorted by the most efficient.
	finalAverages = averageForAlgo(averageArrayInstances)
	print("Count Average")
	pprint(sorted(finalAverages[0].items(), key=lambda x: x[1]))
	print("Fitness Average")
	pprint(sorted(finalAverages[1].items(), key=lambda x: x[1]))
	

main()