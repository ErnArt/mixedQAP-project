#! /bin/bash

timeLimit=1

echo "instance, seed, count, fitness" > resultRandomSearch.csv
for file in ../instances/*
	do
		for seed in {1..30}
			do
				result=$(../code/cpp/build/randomSearch $file $seed $timeLimit)
				arrayResult=(${result//,/ })
				line=$file", "$seed", "${arrayResult[0]}", "${arrayResult[1]}
				echo $line >> resultRandomSearch.csv
				echo "Finished "$file
			done
	done

echo "instance, seed, count, fitness" > resultHillClimber.csv
for file in ../instances/*
	do
		for seed in {1..30}
			do
				result=$(../code/cpp/build/hillClimberExe $file $seed $timeLimit)
				arrayResult=(${result//,/ })
				line=$file", "$seed", "${arrayResult[0]}", "${arrayResult[1]}
				echo $line >> resultHillClimber.csv
				echo "Finished "$file
			done
	done

echo "instance, seed, count, fitness" > resultILS.csv
for file in ../instances/*
	do
		for seed in {1..30}
			do
				result=$(../code/cpp/build/ilsExe $file $seed $timeLimit)
				arrayResult=(${result//,/ })
				line=$file", "$seed", "${arrayResult[0]}", "${arrayResult[1]}
				echo $line >> resultILS.csv
				echo "Finished "$file
			done
	done

echo "instance, seed, count, fitness" > resultES.csv
for file in ../instances/*
	do
		for seed in {1..30}
			do
				result=$(../code/cpp/build/esExe $file $seed $timeLimit)
				arrayResult=(${result//,/ })
				line=$file", "$seed", "${arrayResult[0]}", "${arrayResult[1]}
				echo $line >> resultES.csv
				echo "Finished "$file
			done
	done

mail -s "Résultats de l'algorithme" mail.com <<< "Voilà les résultats de l'algorithme." -A resultRandomSearch.csv -A resultHillClimber.csv -A resultILS.csv